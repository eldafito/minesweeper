package com.dgianessi.minesweeper.shared

import com.dgianessi.minesweeper.game.viewmodel.GameViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single { Config(rowCount = 8, columnCount = 8, mineCount = 15) }

    viewModel {
        GameViewModel(
            config = get()
        )
    }
}