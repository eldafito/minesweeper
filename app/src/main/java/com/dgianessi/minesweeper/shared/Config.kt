package com.dgianessi.minesweeper.shared

data class Config(
        val rowCount: Int,
        val columnCount: Int,
        val mineCount: Int
)