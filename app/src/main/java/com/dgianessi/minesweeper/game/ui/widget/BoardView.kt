package com.dgianessi.minesweeper.game.ui.widget

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.dgianessi.minesweeper.R
import com.dgianessi.minesweeper.game.model.Board
import com.dgianessi.minesweeper.game.model.Box
import timber.log.Timber
import kotlin.math.max
import kotlin.math.min

class BoardView@JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : LinearLayout(context, attrs, defStyleAttr) {

    var size: Pair<Int,Int> = Pair(0,0)
        set(value) {
            field = value
            refreshSize(width, height)
        }

    var board: Board? = null
        set(value) {
            field = value
            refreshBoard()
        }

    var listener: ((r: Int, c: Int) -> Unit)? = null

    init {
        orientation = VERTICAL
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        refreshSize(w, h)
    }

    private fun refreshSize(w: Int, h: Int){
        Timber.d("refreshSize($w,$h) rows = ${size.first} col = ${size.second}")
        removeAllViews()
        if (w <= 0 || h <= 0) return
        val minSize = min(w, h)
        val rowCont = size.first
        val columnCount = size.second
        val maxBoardSize = max(rowCont, columnCount)
        val boxSize = minSize / maxBoardSize
        repeat(rowCont) { r ->
            val rowView = LinearLayout(context).apply {
                orientation = HORIZONTAL
            }
            repeat(columnCount) { c ->
                val textView = TextView(context).apply {
                    gravity = Gravity.CENTER
                    setOnClickListener { listener?.invoke(r,c) }
                    setTextAppearance(android.R.style.TextAppearance_Medium)
                    setTypeface(null, Typeface.BOLD)
                }
                board?.boxes?.getOrNull(r)?.getOrNull(c)?.let { box ->
                    fillBox(textView, board!!.isCovered(r,c), box)
                }
                val boxLayParams = LayoutParams(boxSize, boxSize)
                rowView.addView(textView, boxLayParams)
            }
            val rowLayParams = LayoutParams(boxSize * columnCount, boxSize).also { lp ->
                lp.gravity = Gravity.CENTER_HORIZONTAL
            }
            addView(rowView, rowLayParams)
        }
    }

    private fun refreshBoard(){
        Timber.d("refreshBoard $board")
        val board = board ?: return
        if (board.boxes.size != childCount) return
        board.boxes.forEachIndexed { r, row ->
            val rowView = getChildAt(r) as LinearLayout
            row.forEachIndexed { c, boxState ->
                (rowView.getChildAt(c) as TextView).run { fillBox(this, board.isCovered(r,c), boxState) }
            }
        }
    }

    private fun fillBox(textView: TextView, isCovered: Boolean, box: Box){
        textView.apply {
            post {
                if (isCovered) {
                    setBackgroundResource(R.drawable.bg_box_covered)
                    text = null
                } else {
                    text = when (box) {
                        Box.Mine -> "\uD83D\uDCA3"
                        Box.Empty -> null
                        is Box.Hint -> box.value.toString()
                    }
                    setBackgroundResource(R.drawable.bg_box_discovered)
                    setTextColor(ContextCompat.getColor(context, when(box){
                        is Box.Hint -> when (box.value) {
                            1 -> R.color.blue_800
                            2 -> R.color.green_800
                            else -> R.color.red_800
                        }
                        else -> R.color.black
                    }))
                }
            }
        }
    }

}