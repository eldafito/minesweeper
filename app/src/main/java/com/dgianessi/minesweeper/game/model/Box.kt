package com.dgianessi.minesweeper.game.model

sealed class Box {
    object Mine: Box()
    object Empty: Box()
    class Hint(val value: Int): Box()

    override fun toString() =
        when(this){
            Mine -> "[X]"
            Empty -> "[ ]"
            is Hint -> "[$value]"
        }

}