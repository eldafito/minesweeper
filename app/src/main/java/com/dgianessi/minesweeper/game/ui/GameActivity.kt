package com.dgianessi.minesweeper.game.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.dgianessi.minesweeper.R
import com.dgianessi.minesweeper.game.viewmodel.GameViewModel
import com.dgianessi.minesweeper.game.viewmodel.GameViewModel.*
import com.dgianessi.minesweeper.shared.Config
import kotlinx.android.synthetic.main.activity_game.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.NumberFormat

class GameActivity : AppCompatActivity(R.layout.activity_game) {

    private val config: Config by inject()
    private val viewModel: GameViewModel by viewModel()

    private val numberFormat = NumberFormat.getIntegerInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        game_board_view.size = Pair(config.rowCount, config.columnCount)
        game_new_btn.setOnClickListener {
            viewModel.newGame()
        }
        viewModel.state.observe(this, stateObserver)
    }

    private val stateObserver = Observer<State> { state ->
        game_ovelay.isVisible = !state.isPlaying
        game_new_btn.isVisible = !state.isPlaying
        game_board_view.board = state.board
        game_board_view.listener = if (state.isPlaying) discoverListener else null
        game_score_txt.text = state.score?.let { s ->
            "\uD83C\uDFAE ${numberFormat.format(s)}"
        }
        game_time_txt.text = state.time?.let { t ->
            "\u23F0 ${numberFormat.format(t)}"
        }
    }

    private val discoverListener: (r: Int, c: Int) -> Unit = { r, c ->
        viewModel.discoverBox(r,c)
    }

}