package com.dgianessi.minesweeper.game.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.dgianessi.minesweeper.game.model.Board
import com.dgianessi.minesweeper.game.model.Box
import com.dgianessi.minesweeper.shared.Config
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.concurrent.TimeUnit

class GameViewModel(
    private val config: Config
) : ViewModel() {

    private var ticker: ReceiveChannel<Unit>? = null

    private val intents = Channel<Intent>(Channel.UNLIMITED)
    private val _state = MutableStateFlow(State())
    val state = _state.asLiveData()

    init {
        viewModelScope.launch {
            intents.consumeAsFlow().collect { i ->
                val prevState = _state.value
                val newState = when (i) {
                    Intent.NewGame -> {
                        startTicker()
                        State(board = generateBoard(), isPlaying = true, score = 0, time = 0)
                    }
                    is Intent.Select -> if(prevState.isPlaying) {
                        val newBoard = select(prevState.board!!, i.r, i.c)
                        prevState.copy(board = newBoard, score = newBoard.discoveredCount)
                    }
                    else throw IllegalStateException()
                    Intent.Boom -> if(prevState.isPlaying) {
                        stopTicker()
                        prevState.copy(board = prevState.board?.apply { discoverAll() }, isPlaying = false)
                    }
                    else throw IllegalStateException()

                    Intent.Tick -> if(prevState.isPlaying)
                        prevState.copy(time = prevState.time?.plus(1))
                    else prevState
                }
                Timber.d("New intent $i changed state from $prevState to $newState")
                _state.value = newState
            }
        }
    }

    private fun generateBoard(): Board {
        val boxes = Array(config.rowCount) { Array<Box>(config.columnCount) { Box.Empty } }

        val monoDimenBoard = MutableList(config.rowCount * config.columnCount) { it }
        repeat(config.mineCount) {
            val mine = monoDimenBoard.random()

            val r = mine / config.columnCount
            val c = mine % config.columnCount

            boxes[r][c] = Box.Mine

            val nearRows = listOf(r - 1, r, r + 1).filter { it in 0 until config.rowCount }
            val nearColumns = listOf(c - 1, c, c + 1).filter { it in 0 until config.columnCount }

            nearRows.forEach { nr ->
                nearColumns.forEach { nc ->
                    boxes[nr][nc] = when (val box = boxes[nr][nc]) {
                        Box.Mine -> Box.Mine
                        Box.Empty -> Box.Hint(1)
                        is Box.Hint -> Box.Hint(box.value.plus(1))
                    }
                }
            }
            monoDimenBoard.remove(mine)
        }

        return Board(boxes).also {
            Timber.d(it.toString())
        }
    }

    private fun select(board: Board, r: Int, c: Int): Board {
        Timber.d("Selecting ($r,$c)")
        board.discover(r,c)
        return when (board[r,c]) {
            Box.Empty -> {
                val nearRows = listOf(r - 1, r, r + 1).filter { it in 0 until config.rowCount }
                val nearColumns = listOf(c - 1, c, c + 1).filter { it in 0 until config.columnCount }
                var newBoard = board
                nearRows.forEach { nr ->
                    nearColumns.forEach { nc ->
                        if (board.isCovered(nr,nc)) newBoard = select(board, nr, nc)
                    }
                }
                newBoard
            }
            Box.Mine -> {
                intents.offer(Intent.Boom)
                board
            }
            else -> board
        }
    }

    fun newGame(){
        intents.offer(Intent.NewGame)
    }

    fun discoverBox(r: Int, c: Int){
        intents.offer(Intent.Select(r, c))
    }

    private fun startTicker(){
        ticker?.cancel()
        ticker = ticker(TimeUnit.SECONDS.toMillis(1), 0).also { t ->
            viewModelScope.launch {
                t.consumeAsFlow().collect {
                    intents.send(Intent.Tick)
                }
            }
        }
    }

    private fun stopTicker(){
        ticker?.cancel()
    }

    data class State(
        val board: Board? = null,
        val isPlaying: Boolean = false,
        val score: Int? = null,
        val time: Int? = null
    )

    sealed class Intent {
        object NewGame: Intent()
        object Boom: Intent()
        object Tick: Intent()
        class Select(val r: Int, val c: Int): Intent()

        override fun toString() =
            when(this){
                NewGame -> "NEW_GAME"
                Boom -> "BOOM"
                Tick -> "TICK"
                is Select -> "SELECT($r,$c)"
            }
    }

}