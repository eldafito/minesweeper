package com.dgianessi.minesweeper.game.model

class Board(val boxes: Array<Array<Box>>){

    companion object {
        val EMPTY = Board(emptyArray())
    }

    private val coverage = Array(boxes.size) { r -> Array(boxes[r].size) { true } }

    operator fun get(r: Int,c: Int) = boxes[r][c]

    val discoveredCount get() = coverage.fold(0) { acc, row ->
        acc + row.count { !it }
    }

    fun isCovered(r: Int,c: Int) = coverage[r][c]

    fun discover(r: Int, c: Int) {
        coverage[r][c] = false
    }

    fun discoverAll(){
        coverage.forEach { row ->
            repeat(row.size) { row[it] = false }
        }
    }

    override fun toString() =
        boxes.fold("Board:\r\n") { acc, r ->
            "$acc\r\n${r.joinToString(separator = " ")}"
        }

}