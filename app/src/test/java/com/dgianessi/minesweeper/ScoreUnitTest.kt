package com.dgianessi.minesweeper

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.dgianessi.minesweeper.game.viewmodel.GameViewModel
import com.dgianessi.minesweeper.game.viewmodel.GameViewModel.State
import com.dgianessi.minesweeper.shared.Config
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.MockitoAnnotations

class ScoreUnitTest {

    private val repeatCount = 400

    private val config = Config(
        rowCount = 8,
        columnCount = 8,
        mineCount = 15
    )

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Captor
    private lateinit var captor: ArgumentCaptor<State>

    private val observer = mock<Observer<State>>()
    private lateinit var viewModel: GameViewModel

    @Before
    fun setUp() {
        Dispatchers.setMain(Dispatchers.Unconfined)
        MockitoAnnotations.initMocks(this)
        viewModel = GameViewModel(config = config)
    }

    @Test
    fun scoreCount() {
        viewModel.newGame()
        viewModel.state.observeForever(observer)
        repeat(repeatCount) {
            verify(observer, atLeastOnce()).onChanged(captor.capture())
            val state = captor.value
            if (state.isPlaying) {
                assert(state.score == state.board!!.discoveredCount)

                val randRow = (0 until config.rowCount).random()
                val randCol = (0 until config.columnCount).random()

                viewModel.discoverBox(randRow, randCol)
            }
            else viewModel.newGame()
        }

    }

}