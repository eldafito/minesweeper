package com.dgianessi.minesweeper

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.dgianessi.minesweeper.game.model.Board
import com.dgianessi.minesweeper.game.model.Box
import com.dgianessi.minesweeper.game.viewmodel.GameViewModel
import com.dgianessi.minesweeper.game.viewmodel.GameViewModel.State
import com.dgianessi.minesweeper.shared.Config
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.MockitoAnnotations

class BoardUnitTest {

    private val repeatCount = 100

    private val config = Config(
        rowCount = 8,
        columnCount = 8,
        mineCount = 15
    )

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Captor
    private lateinit var captor: ArgumentCaptor<State>

    private val observer = mock<Observer<State>>()
    private lateinit var viewModel: GameViewModel


    @Before
    fun setUp() {
        Dispatchers.setMain(Dispatchers.Unconfined)
        MockitoAnnotations.initMocks(this)
        viewModel = GameViewModel(config = config)
    }

    @Test
    fun mineCount() {
        repeat(repeatCount) {
            val board = getBoard()
            println(board)

            val mineCount = board.boxes.fold(0) { rowAcc, row ->
                rowAcc + row.fold(0) { acc, box -> if (box == Box.Mine) acc.plus(1) else acc }
            }
            assert(mineCount == config.mineCount)
        }
    }


    @Test
    fun emptyCheck() {
        repeat(repeatCount) {
            val board = getBoard()
            board.boxes.forEachIndexed { r, row ->
                row.forEachIndexed { c, box ->

                    val nearRows = listOf(r - 1, r, r + 1)
                        .filter { it in 0 until config.rowCount }
                    val nearColumns = listOf(c - 1, c, c + 1)
                        .filter { it in 0 until config.columnCount }

                    if (box == Box.Empty) {
                        nearRows.forEach { nr ->
                            nearColumns.forEach { nc ->
                                assert(board[nr, nc] != Box.Mine)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun getBoard(): Board {
        viewModel.newGame()
        viewModel.state.observeForever(observer)
        verify(observer, atLeastOnce()).onChanged(captor.capture())
        return captor.value.board!!
    }

}