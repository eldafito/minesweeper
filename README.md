# Minesweeper 💣

Simple Android project able to play this famous game.

## Architecture

It was decided to follow the MVI architectural pattern.
MVI stands for Model-View-Intent.

## Technologies

1. [Kotlin](https://kotlinlang.org/) main programming language
2. [Koin](https://insert-koin.io) for DI
3. [Coroutines](https://kotlinlang.org/docs/reference/coroutines/coroutines-guide.html) and [Flows](https://kotlinlang.org/docs/reference/coroutines/flow.html) for reactive programming
4. [Junit](https://junit.org/) and [mockito-kotlin](https://github.com/nhaarman/mockito-kotlin) for Unit Test

## Approach

Pay more attention to clean architecture rather than UI

## Conclusion

This project was developed in the evenings after work so I ask forgiveness in advance for possible more or less serious bugs 😄